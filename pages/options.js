var DebbugsEnhancementSuiteOptions = [
    {
      "name": "pretty_uris"
    , "title": "Pretty URIs"
    , "description": "Try and use nicer and shorter URI scheme where possible."
    , "image": false
    , "default": true
  }
  , {
      "name": "collapse_email_signatures"
    , "title": "Collapse email signatures"
    , "description": "Hide email signatures behind expanding links."
    , "image": false
    , "default": true
  }
  , {
      "name": "spam_report_links"
    , "title": "Spam report"
    , "description": "Remove links to report spam from bug pages."
    , "image": false
    , "default": true
  }
  , {
      "name": "copyright_notices"
    , "title": "Copyright notices"
    , "description": "Hide copyright notices from the footers of the page to save space."
    , "image": false
    , "default": true
  }
  , {
      "name": "clean_title"
    , "title": "Clean title"
    , "description": "Remove unnecessary and/or duplicated elements of page titles."
    , "image": false
    , "default": true
  }
  , {
      "name": "unnecessary_headers"
    , "title": "Unnecessary headers"
    , "description": "Remove some 'obvious' headers such as \"To: Debian Bug Tracking System <submit@bugs.debian.org>\" etc. when displaying messages."
    , "image": false
    , "default": true
  }
  , {
      "name": "control_messages"
    , "title": "Control messages"
    , "description": "Simplify the language and structure of \"control\" messages."
    , "image": false
    , "default": true
  }
  , {
      "name": "collapse_bug_headers"
    , "title": "Collapse bug headers"
    , "description": "Coalesce and concatenate a number of elements on a bug's page to save vertical space."
    , "image": false
    , "default": true
  }
  , {
      "name": "collapse_package_headers"
    , "title": "Collapse package headers"
    , "description": "Coalesce and concatenate a number of elements on package summary pages to save vertical space."
    , "image": false
    , "default": true
  }
  , {
      "name": "package_prefixes"
    , "title": "Package name prefixes"
    , "description": "Remove the package name prefixes (eg. \"name: new upstream release\" from bug titles in lists where they match the current package."
    , "image": false
    , "default": true
  }
]
