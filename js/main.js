function DebbugsEnhancementSuite(options) {

  // Configuration ////////////////////////////////////////////////////////////

  var defaults = {};

  $.each(DebbugsEnhancementSuiteOptions, function() {
    defaults[this.name] = this['default'];
  });

  options = $.extend({}, defaults, options);

  // Utilities ////////////////////////////////////////////////////////////////

  $.extend({
    option: function (option, handler) {
      if (options[option] !== false) {
        handler();
      }
    },
    endsWith: function (haystack, needle) {
      return haystack.length >= needle.length &&
        haystack.substr(haystack.length - needle.length) == needle;
    },
    pathnameEndsWith: function (suffix) {
      return $.endsWith(window.location.pathname, suffix);
    },
  });

  // Features /////////////////////////////////////////////////////////////////

  $.option('pretty_uris', function () {
    var m = window.location.href
      .match(/https:\/\/bugs.debian.org\/cgi-bin\/bugreport.cgi\?bug=(\d+)(#.*)?$/);
    if (!m) return;

    // Replace links within the page
    for (var x of document.getElementsByTagName("a")) {
      var href = x.getAttribute("href");
      if (href && href.match(/^[^:]+\.cgi/)) {
          x.setAttribute('href', "/cgi-bin/" + href);
      }
    }

    // Set the "address bar" URI.
    history.replaceState({}, "", "/" + m[1] + window.location.hash);
  });

  $.option('clean_title', function () {
    var h1 = $('body > h1');

    h1
      .html(h1.html().replace(/^Debian Bug report logs(:| -) /, ''))
      .find('br')
      .remove()
      ;

    document.title = h1.text();
  });

  $.option('package_prefixes', function () {
    var match = document.title.match(/ package (\S+)$/)

    if (match == null) {
      return;
    }

    var needle = match[1] + ": ";

    $('div.shortbugstatus a[href^=bugreport]').each(function() {
      var elem = $(this);
      var haystack = $(this).text();

      if (haystack.indexOf(needle) == 0) {
        elem.text(haystack.substring(needle.length));
      }
    });
  });

  $.option('collapse_bug_headers', function () {
    var wrapper = $('body > div.msgreceived p:contains("View this report as a")')
      .css('margin-top', '8px')
      ;

    function append(elem) {
      wrapper.append(' &bull; ');
      wrapper.append(elem.html());
      elem.remove();
    }
    append($('body > p:contains("Toggle useless messages")'));
    append($('body > p:contains("Reply or subscribe to this bug.")'));
  });

  $.option('collapse_package_headers', function () {
    var links = $('body > p:contains("You may want to refer to the")');
    if (links.length == 0) {
      return;
    }

    links[0].firstChild.nodeValue = "Binary packages: ";
    links.remove();

    var wrapper = $('body > p:contains("Maintainers for ")')
      .css('margin-top', '8px')
      .append(' ')
      .prepend(links.html())
      ;

    $('body > p:contains("You might like to refer to the")').remove();
    $('body > p:contains("If you find a bug not listed here, please")').remove();
  });

  $.option('unnecessary_headers', function () {
    $('.headers .header').each(function() {
      var elem = $(this);

      if (elem.text() === "To: Debian Bug Tracking System <submit@bugs.debian.org>") {
        elem.hide();
      }
    });
  });

  $.option('control_messages', function () {
    // Just use verbs, etc.
    $('div.msgreceived p').each(function() {
      var elem = $(this);

      var html = elem.html();
      html = html.replace(/Request was from/, 'by')
      html = html.replace(/to <code>control@bugs.debian.org<\/code>./, '')
      elem.html(html);

      elem.css({
          'width': 'inherit'
      });
    });
  });

  $.option('copyright_notices', function () {
    $('address')
      .hide()
      .prev('hr')
      .hide()
      ;
  });

  $.option('spam_report_links', function () {
    $('p.msgreceived > a[href*="/cgi-bin/bugspam.cgi?bug="]')
      .parent()
      .hide()
      .prev('hr')
      .hide()
      ;
  });

  $.option('collapse_email_signatures', function () {
    $('pre.message').each(function() {
      var elem = $(this);

      elem.html(elem.html().replace(/\n(-- \n.*)/s, function (m, c) {
        elem.css('margin-bottom', '0');

        $('<a href="#">[&hellip;]</a>')
          .css({
              'text-decoration': 'none'
            , 'white-space': 'pre'
            , 'font-size': '12px'
          })
          .on('click', function (e) {
            var elem = $(this);
            e.preventDefault();

            elem
              .hide()
              .prev()
              .find('span')
              .show()
              ;
          })
          .insertAfter(elem)
          ;

        return '<span style="display: none;">\n' + c + '</span>';
      }));
    });
  });
}
