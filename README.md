# Debbugs Enhancement Suite

A Chrome extension to enhance using [Debbugs](https://en.wikipedia.org/wiki/Debbugs).

## Installation from Git

    * Clone (or otherwise download) the contents of this repository.

    * In Chrome, go to "Tools > Extensions".

    * Ensure "Developer mode" is ticked.

    * Click "Load unpacked extension" and select the directory you have stored
      this code.

    * Refresh any page on Debbugs - the extension will now be active.

## Disclaimer

This software is not endorsed by Debbugs, or rather: please do not ask them for
support for this extension.
